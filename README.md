# Python Minis #

A collection of small Python scripts I will use to replace you.

AKA, automating the boring stuff.

* thumbnail.py - a script to programmatically generate thumbnails from a collection of images
  * first scales image down to a size corresponding with the smaller dimension being *foo* pixels, preserving aspect ratio
  * then performs a center-crop of the smaller image
  * then saves a copy of the image with `-thumb` inserted just before the file extension.

* meeting-email-helper
  * Generates email subject lines & bodies for scheduled monthly meeting reminders
  * Depends on a meeting schedule that is always the {nth} {weekday} of the month
  * Calculates meeting date, date agenda items are due, and date email should be sent for each month
  * Facilitates scheduling emails in advance allowing easy copy/paste of pre-generated subject and body for consistency
  * Depends on several non-default libraries. From the meeting-email-helper directory, do `python3 -m pip install -r requirements.txt`
      * You may wish to employ a [python environment](https://docs.python.org/3/library/venv.html) here
  * Edit the template (./templates/message.txt) before running to optionally to customize the email body text
  * Run python3 script.py and the script will ask you for the parameters it needs. It will save the file to the directory you invoke the script from.
