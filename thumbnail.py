"""Crop and reduce size of all images found. 

Restricted to files that don't include the string 'thumb' in the filename.
"""


import sys
import os
import getopt
import cv2
import pdb

file_path = False

# check opts to see if we passed a custom file path
try:
    opts, args = getopt.getopt(sys.argv[1:], "p:")

except Exception as e:
    print("Unable to get args. Will continue with default settings.")
finally:
    for opt, arg in opts:
        if opt in ["-p"]:
            file_path = args.strip(" ")
# we only want to make thumbnails from image files, so let's specify those extensions
extensions = [".jpg", ".jpeg", ".png", ".svg", ".webp", ".gif"]
print("Looking for:")
for e in extensions:
    print(e)
try:
    y = 0
    if file_path == False:
        files = os.listdir()  # get the list of files from the local directory
    else:
        files = os.listdir(file_path)  # get the list of files from the path specified
        files.sort()  # put them in order
    while y < len(files):
        # only start with files that aren't thumbs
        if "thumb" not in files[y]:

            print("Found one to check: " + files[y])
            basename, ext = os.path.splitext(files[y])
            print("This file is a " + ext.lower())
            if ext.lower() in extensions:
                match = False
                i = 0
                while i < len(files) and match == False:
                    bname, ex = os.path.splitext(files[i])
                    test_name = basename + "-thumb"
                    print("Comparing " + bname + " against " + test_name)
                    if bname == test_name:
                        print("Found a match for it: " + files[i] + "On to the next!")
                        match = True
                    i += 1
                img = False
                if match == False:
                    try:
                        # the non-thumb file doesn't have a matching -thumb file, so let's make one
                        print("We need to thumbnail this one. Please stand by...")
                        if file_path:
                            full_path = file_path + files[y]
                            img = cv2.imread(full_path)
                        else:
                            img = cv2.imread(files[y])

                        width, height = img.shape[1], img.shape[0]
                        crop_width = 300 if 300 < img.shape[1] else img.shape[1]
                        crop_height = 300 if 300 < img.shape[0] else img.shape[0]

                        mid_x, mid_y = int(width / 2), int(height / 2)
                        cw2, ch2 = int(crop_width / 2), int(crop_height / 2)
                        crop_img = img[
                            mid_y - ch2 : mid_y + ch2, mid_x - cw2 : mid_x + cw2
                        ]
                        thumb_filename = basename + "-thumb" + ext
                        cv2.imwrite(thumb_filename, crop_img)
                        print("Saved " + str(thumb_filename))
                    except Exception as e:
                        print("I couldn't process the image.")

        y += 1  # increment to the next filename

except Exception as e:
    print("I couldn't get the list of files.")
