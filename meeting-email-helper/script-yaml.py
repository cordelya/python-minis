'''Monthly Meeting Reminder Email Generator

Creates a text file containing copy & paste ready text for scheduling an entire
year's worth of meeting reminder emails, including when to schedule the email
for, what the meeting date will be, what date agenda items are due by, where 
to send the agenda items, and how to request the meeting link if virtual.

Generates 12 email blocks - one for each month, Jan - Dec, of the specified
calendar year. If you need blocks that span multiple years, you will need to
run the script once for each year. The file name will include the year so you
can generate for multiple years without overwriting files.

Take the resulting text file and you can copy & paste subject lines and email
bodies into your email composer before scheduling each email.
'''

import yaml
import calendar
import inflect
import datetime
from jinja2 import Environment, FileSystemLoader

def main(
    templatefile="message.txt",
    yamlfile="meeting.yaml"
):

    DEBUG = False

    # If you want to edit the subject lines or email body text, the template 
    # is found in templates/message.txt. 
    # The template includes some template tags, identified as text wrapped in
    # double curly braces ({{ tag_name }}). These must match variable names
    # and if you edit the template, make sure these are kept intact or
    # this script will throw an error.

    # some tech setup
    p = inflect.engine()
    c = calendar.Calendar(firstweekday=calendar.SUNDAY)

    # define day of week constants to match the calendar module's mapping
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    # Import params from the yaml file
    with open(yamlfile, 'r') as file:
        params = yaml.safe_load(file)
    if DEBUG:
        print(params)

    year = params['year']
    org = params['org']
    meeting_dow = params['dow']
    meeting_wom = params['week']
    time = params["time"]
    agenda_due_days = params['agenda_due']
    send_email = params['send_email']
    meeting_link = params['meeting_link']
    mod_name = params['mod_name']
    mod_email = params['mod_email']
    agenda_name = params['agenda_name']
    agenda_email = params['agenda_email']
    event = params['event']

    if DEBUG:
        print("{} meeting on {} during week {} in {}.".format(event, meeting_dow, meeting_wom, year))
    
    ## initialize the file (this will overwrite any pre-existing file with the same name)

    # specify naming format of file
    filename = "{}-meetings-{}.txt".format(year, org)

    # specify the text for the first line of the file
    title = "{} Scheduled Meeting Reminder Emails for {}\n\n---\n\n".format(org, year)

    # initialize / open the file and write the first line
    with open(filename, "w") as f:
        f.write(title)

    # adjust meeting_week_of_month to use numbering from 0
    meeting_wom = int(meeting_wom) - 1

    environment = Environment(loader=FileSystemLoader("templates/"))
    template = environment.get_template(templatefile)
    
    # populate the file with customized email blocks, 
    # one for each month in the specified calendar year
    for x in range(12):
        # calculate email, agenda due, and meeting dates
        month = (
            x + 1
        )  # x begins from 0 where the calendar module expects numbering to begin from 1, so we increase x by 1 to keep up
        monthname = calendar.month_name[
            month
        ]  # gets the English month name based on the month number
        # get the nth weekday of the year based on what is given in meeting_day_of_week and meeting_week_of_month
        monthcal = c.monthdatescalendar(year, month)
        weekday = eval(meeting_dow.upper())

        meeting_date = [
            day
            for week in monthcal
            for day in week
            if day.weekday() == weekday and day.month == month
        ][meeting_wom]

        # get the ordinal for the meeting date
        meeting_date_int = int(meeting_date.strftime("%d"))
        meeting_date_ord = p.ordinal(meeting_date_int)
        if DEBUG:
            print("The meeting date is {}".format(meeting_date_int))

        # calculate the date and day of the week the email should be scheduled for
        days = datetime.timedelta(send_email)
        send_email_date = meeting_date - days
        send_email_month = send_email_date.strftime("%B")
        send_email_year = send_email_date.strftime("%Y")
        send_email_dow = send_email_date.strftime("%A")
        send_email = int(send_email_date.strftime("%d"))

        # calculate the ordinal date and day of week that agenda items are due
        days = datetime.timedelta(agenda_due_days)
        agenda_due_date = meeting_date - days
        agenda_due = int(agenda_due_date.strftime("%d"))
        if DEBUG:
            print("The agenda is due on {}.".format(agenda_due))
        agenda_month = agenda_due_date.strftime("%B")
        agenda_year = agenda_due_date.strftime("%Y")
        #agenda_weekday_num = calendar.weekday(year, month, agenda_due)
        agenda_weekday = agenda_due_date.strftime("%A")
        agenda_due_ord = p.ordinal(agenda_due)

        # write each month to a text file
        # the template for this is found in templates/message.txt
        
        content = template.render(
            year=year,
            org=org,
            mod_name=mod_name,
            mod_contact=mod_email,
            time=time,
            agenda_name=agenda_name,
            agenda_contact=agenda_email,
            event=event,
            send_email_dow=send_email_dow,
            send_email=send_email,
            send_email_month=send_email_month,
            send_email_year=send_email_year,
            meeting_date=int(meeting_date.strftime("%d")),
            meeting_date_ord=meeting_date_ord,
            meeting_link=meeting_link,
            agenda_weekday=agenda_weekday,
            agenda_due=agenda_due,
            agenda_due_ord=agenda_due_ord,
            agenda_month=agenda_month,
            agenda_year=agenda_year,
            month=monthname,
            dow=meeting_dow,
        )
        # Append the block for the current month to the end of the file.
        with open(filename, "a") as f:
            f.write(content)

    # Add a footer
    footer = (
        "Built using Cordelya's Meeting Email Scheduling Helper \
        https://gitlab.com/cordelya/python-minis/\n"
    )
    with open(filename, "a") as f:
        f.write(footer)

if __name__ == "__main__":
    main()
