# Meeting Email Helper

This script generates a text file which contains send dates, subject lines, 
and body texts for meetings which occur on a monthly basis when the meeting 
is always on the same weekday of the same week of the month (ie the Second 
Thursday of every month). 

The script generates these for an entire calendar year at once in one file.

It will ask you for input regarding:

* Which year you are generating emails for
* Which day of the week the meetings occur on
* Which week of the month the meetings occur during
* The number of days before the meeting when agenda items are due
* The number of days before the meeting to schedule the email to be sent
* The name and email address of the person to contact for meeting technical info (meeting join link, etc)
* The name and email address of the person to send agenda items to
* The name of the organization that is having the meetings
* The kind of meeting it is (ie "Committee" or "Business"). Exclude the word "meeting" from your answer to this one.

To run this script, do the following on a machine running a recent version of Python 3:

Do one of:
* download and unzip this repository and navigate to the ./python-minis/meeting-email-helper directory in a terminal environment, or
* if `git` is available, do `git clone https://gitlab.com/cordelya/python-minis` and navigate into the ./python-minis/meeting-email-helper directory

Once you have a local copy and are in the correct directory:

* Optional: [initiate a virtual environment](https://docs.python.org/3/library/venv.html)
* do `python3 -m pip install -r requirements.txt` to install the libraries the script needs
* review the message template in templates/message.txt and make any adjustments you need

## The Typer Option

* do `python3 script-typer.py` and supply the prompts with answers
* On a successful run, you will find a new .txt file in the directory. If you run this script from a different directory, 
the file will save to that location instead.
* On any subsequent successful run from the same directory, the script will overwrite any previously generated file for the same year and organization.


## The YAML Option

* Edit meeting.yaml to supply all of the meeting specifics
* do `python3 script-yaml.py`
* On a successful run, you will find a new .txt file in the directory. 
* On any subsequent successful run from the same directory, the script will overwrite any previously generated file for the same year and organization.
