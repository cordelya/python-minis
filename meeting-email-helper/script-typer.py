'''Monthly Meeting Reminder Email Generator

Creates a text file containing copy & paste ready text for scheduling an entire
year's worth of meeting reminder emails, including when to schedule the email
for, what the meeting date will be, what date agenda items are due by, where 
to send the agenda items, and how to request the meeting link if virtual.

Generates 12 email blocks - one for each month, Jan - Dec, of the specified
calendar year. If you need blocks that span multiple years, you will need to
run the script once for each year. The file name will include the year so you
can generate for multiple years without overwriting files.

Take the resulting text file and you can copy & paste subject lines and email
bodies into your email composer before scheduling each email.
'''

import typer
import calendar
import inflect
import datetime
from jinja2 import Environment, FileSystemLoader
from typing_extensions import Annotated

def main(
    year: Annotated[int, typer.Option(prompt="What year should I generate emails for?")],
    meeting_day_of_week: Annotated[str, typer.Option(prompt="On which day of the week (eg 'Tuesday') does this meeting recur?")],
    meeting_week_of_month: Annotated[int, typer.Option(prompt="In which week of the month (eg '1' for the first week) does this meeting recur?")],
    meeting_time: Annotated[str, typer.Option(prompt="At what time does this meeting begin?")],
    agenda_due_days_before_meeting: Annotated[int, typer.Option(prompt="How many days before the meeting are agenda items due?")],
    send_email_days_before_meeting: Annotated[int, typer.Option(prompt="How many days before the meeting should the email be scheduled?")],
    org: Annotated[str, typer.Option(prompt="What is the name of the organization?")],
    meeting_link: Annotated[str, typer.Option(prompt="What is the link to the online meeting? Leave blank if it must be requested from the contact.")],
    mod_name: Annotated[str, typer.Option(prompt="What is the name of the person to contact for meeting details / links?")],
    mod_contact: Annotated[str, typer.Option(prompt="What is the email address of the person to contact for meeting details / links?")],
    agenda_name: Annotated[str, typer.Option(prompt="What is the name of the person to contact with agenda items?")],
    agenda_contact: Annotated[str, typer.Option(prompt="What is the email address of the person to contact with agenda items?")],
    event: Annotated[str, typer.Option(prompt="What type of meeting is this? Example: 'Committee' / don't include the word 'meeting' here")],
):

    DEBUG = False

    # If you want to edit the subject lines or email body text, the template 
    # is found in templates/message.txt. 
    # The template includes some template tags, identified as text wrapped in
    # double curly braces ({{ tag_name }}). These must match variable names
    # and if you edit the template, make sure these are kept intact or
    # this script will throw an error.

    # some tech setup
    p = inflect.engine()
    c = calendar.Calendar(firstweekday=calendar.SUNDAY)

    # define day of week constants to match the calendar module's mapping
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    # initialize the file (this will overwrite any pre-existing file with the same name)

    # specify naming format of file
    filename = "{}-meetings-{}.txt".format(year, org)

    # specify the text for the first line of the file
    title = "{} Scheduled Meeting Reminder Emails for {}\n\n---\n\n".format(org, year)

    # initialize / open the file and write the first line
    with open(filename, "w") as f:
        f.write(title)

    # adjust meeting_week_of_month to use numbering from 0
    meeting_week_of_month = int(meeting_week_of_month) - 1

    environment = Environment(loader=FileSystemLoader("templates/"))
    template = environment.get_template("message.txt")
    
    # populate the file with customized email blocks, 
    # one for each month in the specified calendar year
    for x in range(12):
        # calculate email, agenda due, and meeting dates
        month = (
            x + 1
        )  # x begins from 0 where the calendar module expects numbering to begin from 1, so we increase x by 1 to keep up
        monthname = calendar.month_name[
            month
        ]  # gets the English month name based on the month number
        # get the nth weekday of the year based on what is given in meeting_day_of_week and meeting_week_of_month
        monthcal = c.monthdatescalendar(year, month)
        weekday = eval(meeting_day_of_week.upper())

        meeting_date = [
            day
            for week in monthcal
            for day in week
            if day.weekday() == weekday and day.month == month
        ][meeting_week_of_month]

        # get the ordinal for the meeting date
        meeting_date_int = int(meeting_date.strftime("%d"))
        meeting_date_ord = p.ordinal(meeting_date_int)

        # calculate the date and day of the week the email should be scheduled for
        days = datetime.timedelta(send_email_days_before_meeting)
        send_email_date = meeting_date - days
        send_email_month = send_email_date.strftime("%B")
        send_email_year = send_email_date.strftime("%Y")
        send_email_dow = send_email_date.strftime("%A")
        send_email = int(send_email_date.strftime("%d"))

        # calculate the ordinal date and day of week that agenda items are due
        days = datetime.timedelta(agenda_due_days_before_meeting)
        agenda_due_date = meeting_date - days
        agenda_due = int(agenda_due_date.strftime("%d"))
        if DEBUG:
            print(agenda_due)
        agenda_month = agenda_due_date.strftime("%B")
        agenda_year = agenda_due_date.strftime("%Y")
        agenda_weekday_num = calendar.weekday(year, month, agenda_due)
        agenda_weekday = calendar.day_name[agenda_weekday_num]
        agenda_due_ord = p.ordinal(agenda_due)

        # write each month to a text file
        # the template for this is found in templates/message.txt
        
        content = template.render(
            year=year,
            org=org,
            mod_name=mod_name,
            mod_contact=mod_contact,
            agenda_name=agenda_name,
            agenda_contact=agenda_contact,
            event=event,
            send_email_dow=send_email_dow,
            send_email=send_email,
            send_email_month=send_email_month,
            send_email_year=send_email_year,
            meeting_date=int(meeting_date.strftime("%d")),
            meeting_date_ord=meeting_date_ord,
            meeting_link=meeting_link,
            agenda_weekday=agenda_weekday,
            agenda_due=agenda_due,
            agenda_due_ord=agenda_due_ord,
            agenda_month=agenda_month,
            agenda_year=agenda_year,
            month=monthname,
            dow=meeting_day_of_week,
        )
        # Append the block for the current month to the end of the file.
        with open(filename, "a") as f:
            f.write(content)

    # Add a footer
    footer = (
        "Built using Cordelya's Meeting Email Scheduling Helper \
        https://gitlab.com/cordelya/python-minis/\n"
    )
    with open(filename, "a") as f:
        f.write(footer)

if __name__ == "__main__":
    typer.run(main)
