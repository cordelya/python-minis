# generate a qr code for wifi login
# based on: https://www.geeksforgeeks.org/wi-fi-qr-code-generator-using-python/

# import module
import wifi_qrcode_generator

# variables
ssid = ""  # the network ssid
password = ""  # the network password

# generate Qr code
try:
    wifi_qrcode_generator.wifi_qrcode(ssid, False, "WPA", password)
except Exception as e:
    print("QR Generation Failed")
